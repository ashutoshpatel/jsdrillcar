// import files
import getYearsOfAllCar from '../problem4.js'; 
import inventory from '../inventoryData.js';

// create variable to store all years of car
const carYears = getYearsOfAllCar(inventory);

if(carYears){
    // we can print using directly
    console.log(carYears);
    
    // print using loop
    // for(let index=0; index < carYears.length; index++){
    //     console.log(carYears[index]);
    // }
}
else{
    // if the carYears is null means we do not have any car in inventory
    console.log("Car not found");
}