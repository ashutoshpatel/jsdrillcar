// import files
import getAllBMWAndAudiCar from '../problem6.js';
import inventory from '../inventoryData.js';

// call the function
const BMWAndAudiCar = getAllBMWAndAudiCar(inventory);

if(BMWAndAudiCar){
    // print directly
    console.log(BMWAndAudiCar);

    // print all BMW and Audi Cars using loop
    // console.log("BMW and Audi cars:");
    // for (let index = 0; index < BMWAndAudiCar.length; index++) {
    //     // create variable to store car
    //     let currentCar = BMWAndAudiCar[index];
    //     console.log(`Car ${currentCar.id} is a ${currentCar.car_year} ${currentCar.car_make} ${currentCar.car_model}`);
    // }
}
else{
    // if the BMWAndAudiCar is null means we do not have any car in inventory
    console.log("Car not found");
}