// import files
import findLastCar from '../problem2.js'; 
import inventory from '../inventoryData.js'; 

// call findLastCar function
const lastCar = findLastCar(inventory);

// printing
// if the lastCar is null means we do not have any car in inventory
if(lastCar == null){
    console.log("No car found");
}
else{
    console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`);
}