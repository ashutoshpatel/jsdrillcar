// import files
import findOlderCarsThan2000 from '../problem5.js';
import inventory from '../inventoryData.js';

// call the function
const olderCars = findOlderCarsThan2000(inventory);

if(olderCars){
    // print all older car using directly
    console.log("Older cars:", olderCars);
    
    // print how many car is older then 2000 year
    console.log("Number of older cars:", olderCars.length);
    
    // print using loop
    // for (let index = 0; index < olderCars.length; index++) {
    //     // get current older car
    //     let currentCar = olderCars[index];
    //     console.log(currentCar.id + " " + currentCar.car_make + " " + currentCar.car_model + " " + currentCar.car_year);
    // }
}
else{
    // if the olderCars is null means we do not have any car in inventory
    console.log("Car not found");
}