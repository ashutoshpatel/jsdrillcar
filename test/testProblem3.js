// import files
import listCarModelAlphabetically from '../problem3.js';
import inventory from '../inventoryData.js';

// call the listCarModelAlphabetically function
const carModels = listCarModelAlphabetically(inventory);

if (carModels) {
    // print the car directly
    console.log(carModels);

    // print using for loop
    // for (let index = 0; index < carModels.length; index++) {
    //     console.log(carModels[index]);
    // }
}
else{
    // if the carModels is null means we do not have any car in inventory
    console.log("Car not found");
}