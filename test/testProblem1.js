// import files
import findCarInformation from "../problem1.js";
import inventory from "../inventoryData.js";

// create variable to store data fo id
const dataOfID = findCarInformation(inventory,33);

// if the dataOfID is not a null then print the details
if (dataOfID) {
  console.log(`Car 33 is a ${dataOfID.car_year} ${dataOfID.car_make} ${dataOfID.car_model}`);
}
// if is not a present
else {
  console.log("Car with id number 33 is not present in inventory");
}