function listCarModelAlphabetically(inventory) {

    // if the inventory is empty return null
    if (inventory.length == 0) {
        return null;
    }

    // create empty array to stroe all car model
    let carModels = [];

    // run loop to get all car model
    for (let index = 0; index < inventory.length; index++) {
        carModels.push(inventory[index].car_model);
    }

    // sort the carModels
    carModels.sort();

    return carModels;
}

export default listCarModelAlphabetically;