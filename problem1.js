function findCarInformation(inventory, requireId) {

  // if the inventory is empty return null
  if (inventory.length == 0) {
    return null;
  }

  // run for loop to find requireId in inventory
  for (let index = 0; index < inventory.length; index++) {
    // if the requireId is present in inventory
    if (inventory[index].id === requireId) {
      // return that inventory ID
      return inventory[index];
    }
  }

  // if the requireId is not present in inventory
  return null;
}

export default findCarInformation;