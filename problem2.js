function findLastCar(inventory){

    // if the inventory not have any data
    if(inventory.length == 0){
        return null;
    }

    // run a loop till inventory length
    for(let index=0; index < inventory.length; index++){
        // if the current index is inventory.length-1 means it's our last car
        if(index === inventory.length-1){
            return inventory[index];
        }
    }
}


export default findLastCar;