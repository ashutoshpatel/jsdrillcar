function getAllBMWAndAudiCar(inventory) {

    // if the inventory is empty return null
    if (inventory.length == 0) {
        return null;
    }

    // create array to store all BMW and Audi car details
    let BMWAndAudiCar = [];

    // run loop to get All BMW and Audi Cars
    for (let index = 0; index < inventory.length; index++) {
        // create variable to store car maker
        const carMaker = inventory[index].car_make;
        // check if current carMaker is equal to current inventory car maker
        if (carMaker === "BMW" || carMaker === "Audi") {
            BMWAndAudiCar.push(inventory[index]);
        }
    }

    return BMWAndAudiCar;
}

export default getAllBMWAndAudiCar;