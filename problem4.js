function getYearsOfAllCar(inventory) {

    // if the inventory is empty return null
    if (inventory.length == 0) {
        return null;
    }

    // create array to store all the years of a car
    const carYears = [];

    // run loop to get all years of car
    for (let index = 0; index < inventory.length; index++) {
        carYears.push(inventory[index].car_year);
    }

    return carYears;
}

export default getYearsOfAllCar;