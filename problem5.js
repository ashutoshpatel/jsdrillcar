function findOlderCarsThan2000(inventory) {

    // if the inventory is empty return null
    if (inventory.length == 0) {
        return null;
    }

    // create array to store all older cars details which is older then 2000 year
    let olderCars = [];

    // run a loop to get all older car then 2000 years
    for (let index = 0; index < inventory.length; index++) {
        // if the current year is less than 2000 means it's older car then 2000 years
        if (inventory[index].car_year < 2000) {
            olderCars.push(inventory[index]);
        }
    }

    return olderCars;
}

export default findOlderCarsThan2000;